//
//  glanceAPI.swift
//  Glance
//
//  Created by Professional on 2015-06-10.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AFNetworking
import Haneke

let LOGIN_URL:String = "https://glanceapp.herokuapp.com/api/1.0/user/"
let SIGNUP_URL:String = "https://glanceapp.herokuapp.com/api/1.0/user/login/"

let VIDEOS_URL:String = "https://glanceapp.herokuapp.com/api/1.0/video/"
let APIKEY:String = "Basic bGFsc3RlZjoxMjM="

let BASICURL: String = "https://glanceapp.herokuapp.com/api/1.0"

// GET links
let GETUSER: String = "https://glanceapp.herokuapp.com/api/1.0/user"

let FASTTHUMBPATH: String = "https://s3-us-west-2.amazonaws.com/glanceapp/thumbs/"
let FASTAVATARPATH: String = "https://s3-us-west-2.amazonaws.com/glanceapp/avatars/"
let FASTVIDEOPATH: String = "https://s3-us-west-2.amazonaws.com/glanceapp/videos/"

let GETFAVCITY: String = "https://glanceapp.herokuapp.com/api/1.0/city/"
let UPLOADVIDEO:String = "https://glanceapp.herokuapp.com/api/1.0/video/"
let SHAREVID:String = "https://glanceapp.herokuapp.com/i/video/share"


//Create the delegate protocol
protocol APIDelegate {
    func didReceiveFeeds(result: SwiftyJSON.JSON)
    //func didSucceededLogin()
}

let GlanceAPISingleton = glanceAPI()
let cache = Shared.dataCache
typealias glanceHelperCallBack = ((error : NSError?, result : SwiftyJSON.JSON? ) -> Void)?

class glanceAPI {

    var delegate: APIDelegate?
    var userName: String?
    var passWord: String?
    var callback: glanceHelperCallBack
    let param = [
        "Authorization": "Basic aW1lZDoxMjM="
    ]
    //static let sharedInstance = glanceAPI() // Other way of create a singleton instance
    
    private init() {
        println("API INSTANCE");
    }
    
    func login(username: String, password:String) {
            
        let credential = NSURLCredential(user: username, password: password, persistence: .ForSession)
        Alamofire.request(.GET, "\(LOGIN_URL)/\(username)/\(password)")
            .authenticate(usingCredential: credential)
            .response {(request, response, _, error) in
                if error == nil {
                    println(response)
                    self.userName = username
                    self.passWord = password
                }else{
                    println(error)
                }
        }
        
    }
    
    func loadFeedsVideos(callback: glanceHelperCallBack) {
        
        self.callback = callback
        println("downloading ALL Videos...")
        
        var URL = VIDEOS_URL
        Alamofire.request(.GET, URL, parameters: param)
            .authenticate(user: "remo", password: "123")
            .responseJSON { (request, response, jsonDATA, error) in
                if error != nil{
                    println("OUPS! ERROR:\(error)")
                    //println(response)
                }else{
                    let json = JSON(jsonDATA!)
                    //println(json)
                    //self.cachesVideos(json)
                    self.callback!(error: nil, result: json)
                    
                }
        }
    }
    
    func fetchUserVideos(callback: glanceHelperCallBack) {
        
        self.callback = callback
        println("downloading user Videos...")
        
        var URL = VIDEOS_URL
        Alamofire.request(.GET, URL, parameters: param)
            .authenticate(user: "remo", password: "123")
            .responseJSON { (request, response, jsonDATA, error) in
                if error != nil{
                    println("OUPS! ERROR:\(error)")
                    //println(response)
                }else{
                    let json = JSON(jsonDATA!)
                    self.callback!(error: nil, result: json)
                    
                }
        }
    }
        
    func fetchNearbyVideos() {
        
        var nearbyURL = ""
        var URL = VIDEOS_URL
        URL += nearbyURL
        
        Alamofire.request(.GET, URL, parameters: param)
            .authenticate(user: "remo", password: "123")
            .responseJSON { (request, response, jsonDATA, error) in
                if error != nil{
                    println("OUPS! ERROR:\(error)")
                    //println(response)
                }else{
                    let json = JSON(jsonDATA!)
                    println(json)
                    //self.cachesVideos(json)
                    self.delegate?.didReceiveFeeds(json)
                    
                }
        }
    }
    
    
    func searchCity(callback: glanceHelperCallBack) {
        
        self.callback = callback
        println("Searching for city")

        var URL = GETFAVCITY
        Alamofire.request(.GET, URL, parameters: param)
            .authenticate(user: "remo", password: "123")
            .responseJSON { (request, response, jsonDATA, error) in
                if error != nil{
                    println("OUPS! ERROR:\(error)")
                    //println(response)
                }else{
                    let json = JSON(jsonDATA!)
                    println("CITY RESULT: \(json)")
                    //self.cachesVideos(json)
                    //self.delegate?.didReceiveFeeds(json)
                    self.callback!(error: nil, result: json)
                }
        }
    }
    
    //Fetch Comments
    func fetchComments(callback: glanceHelperCallBack) {
        
        self.postComments()
        
        self.callback = callback
        println("Searching for video comments")
        var videoID = "324"
        var commentURL = ":\(videoID)/comment/"
        var URL = VIDEOS_URL
        URL += commentURL
        var videoIDurl = "https://glanceapp.herokuapp.com/api/1.0/video/322/comment/"
        Alamofire.request(.GET, videoIDurl, parameters: param)
            .authenticate(user: "remo", password: "123")
            .responseJSON { (request, response, jsonDATA, error) in
                if error != nil{
                    println("OUPS! ERROR:\(error)")
                    //println(response)
                }else{
                    let json = JSON(jsonDATA!)
                    println("Comments List RESULT: \(json)")
                    //self.callback!(error: nil, result: json)
                }
        }
    }
    
    //Post Comments
    func postComments() {
        
        /*println("posting comment for video")
        //var videoID = "322"
        
        //var commentURL = "\(videoID)/comment/"
        //var URL = VIDEOS_URL
        //URL += commentURL
        var videoIDurl = "https://glanceapp.herokuapp.com/api/1.0/video/322/comment/"
        let parameters = [
            "Authorization": "Basic aW1lZDoxMjM=",
            "text": "Such a great video! I'm testing"
        ]

        Alamofire.request(.POST, videoIDurl, parameters: parameters)
            .authenticate(user: "remo", password: "123")
            .responseJSON { (request, response, jsonDATA, error) in
                if error != nil{
                    println("OUPS! ERROR:\(error)")
                }else{
                    println(response)
                    //let json = JSON(jsonDATA!)
                    //println("Comments List RESULT: \(json)")
                }
        }*/
        let headers = [
            "authorization": "Basic aW1lZDoxMjM=",
            "content-type": "application/json",
            "accept-encoding": "gzip",
            "user": "remo",
            "password": "123"
        ]
        let parameters = ["text": "What a great video!"]
        
        let postData = NSJSONSerialization.dataWithJSONObject(parameters, options: nil, error: nil)
        
        var request = NSMutableURLRequest(URL: NSURL(string: "https://glanceapp.heroku.com/api/1.0/video/322/comment/")!,
            cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: 10.0)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.HTTPBody = postData
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                println(error)
            } else {
                let httpResponse = response as? NSHTTPURLResponse
                println(httpResponse)
            }
        })
        
        dataTask.resume()
    }
    
    func cachesVideos(result: SwiftyJSON.JSON){
        
        var feedsArray: NSMutableArray = []
        
        //println(result["objects"])
        
        for (index: String, jsonFeed: SwiftyJSON.JSON) in result["objects"] {
            
            var videoPath = jsonFeed["video"].string!
            println("\(videoPath)")
            let videoUrl = NSURL(fileURLWithPath: videoPath)
            
            let URL = NSURL(string: videoPath)!
            let fetcher = NetworkFetcher<NSData>(URL: URL)
            
            cache.fetch(fetcher: fetcher).onSuccess { video in
                // Do something with the video

                let videoData:NSData? = video
                cache.set(value: videoData!, key: videoPath)
                println("video cached")
            }

        }
    }
    
    func downloadVideo(path:String){

    }
    
    func randomStringWithLength (len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        var randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i=0; i < len; i++){
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
    }
    
    func uploadVideo(videoData:NSData, videoURl:NSURL){
        
        let videoPath = "https://d2hsyytjp5izl3.cloudfront.net/videos/shutterstock_v5454419.m3u8"
        var mydata:NSData?
        if let data = (videoPath as NSString).dataUsingEncoding(NSUTF8StringEncoding){
            mydata = data
        }
        
        let testURL = "http://posttestserver.com/post.php?dir=glance2"
        
        let manager = AFHTTPSessionManager()
        var requestSerial:AFHTTPRequestSerializer = AFHTTPRequestSerializer()
        requestSerial.setValue("remo", forHTTPHeaderField: "user")
        requestSerial.setValue("123", forHTTPHeaderField: "password")
        requestSerial.setValue("Basic aW1lZDoxMjM=", forHTTPHeaderField: "Authorization")
        //requestSerial.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer = requestSerial
        manager.session.configuration.HTTPAdditionalHeaders = [
            "Content-Type": "multipart/form-data",
            "Accept-Encoding": "gzip"
        ]
        
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json") as Set<NSObject>
        
        let url = UPLOADVIDEO
        
        var params = [
            "lat": "1.5527778000000001",
            "lng": "15.6405556000000008",
            "headline": "test",
            "description": "test",
            "category": "test"
        ]
        var videoName = randomStringWithLength(10)
        
        manager.POST(UPLOADVIDEO, parameters: params,
            constructingBodyWithBlock: { (data: AFMultipartFormData!) in
                data.appendPartWithFileData(videoData, name: "video", fileName: "\(videoName).mp4", mimeType: "video/mp4")
                //var res = data.appendPartWithFileURL(videoURl, name: "video", error: nil)
                //println("was file added properly to the body? \(res)")
                
            },
            success: { operation, response in
                println("SUCCESS operation: \(operation)")
                //println("SUCCESS operation: \(operation), response: \(response)")
                
                if let encryptedData:NSData = response as? NSData {
                    var jsonString = NSString(data: encryptedData, encoding: NSUTF8StringEncoding)! as String
                    let json = JSON(jsonString)
                    println("json: \(json)")
                    
                }
                
            },
            failure: { operation, error in
                println("FAIL operation: \(operation), error: \(error)")
            }
        )
    }
    
}
