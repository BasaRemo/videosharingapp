//
//  FeedCell.swift
//  Glance
//
//  Created by Professional on 2015-06-09.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Spring
import Haneke
//import ZFModalTransitionAnimator
protocol FeedCellDelegate {
    func showCommentView()
    func showLikeView()
}

class FeedCell: UITableViewCell {

    @IBOutlet weak var locationUserContainer: SpringView!
    @IBOutlet weak var likeCommentContainer: SpringView!
    @IBOutlet weak var overlayLabel: UILabel!
    @IBOutlet var thumbnail:UIImageView!
    var thumbnailImage: UIImage?
    var videoModel:VideoFeedModel?
    var infoViewsShown = false
    var delegate:FeedCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        var longPressGestureRecognizer: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "handleLongPresssGesture:")
        //self.addGestureRecognizer(longPressGestureRecognizer)
        
        var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGestureRecognizer:")
        tapGestureRecognizer.numberOfTapsRequired = 1
        //self.addGestureRecognizer(tapGestureRecognizer)
        
        //Overlay Set Up
        locationUserContainer.animation = "slideDown"
        likeCommentContainer.animation = "slideUp"
        
        locationUserContainer.duration = 0.5
        likeCommentContainer.duration = 0.5
        
        likeCommentContainer.damping = 1
        locationUserContainer.damping = 1
        
        //likeCommentContainer.autohide = true
        //locationUserContainer.autohide = true
        
    }

    func handleLongPresssGesture(gestureRecognizer: UITapGestureRecognizer) {
        locationUserContainer.animate()
        likeCommentContainer.animate()
        locationUserContainer.animation = "fadeOut"
        likeCommentContainer.animation = "fadeOut"
    }
    
    func handleTapGestureRecognizer(gestureRecognizer: UITapGestureRecognizer) {
        println("SlideDown")
        locationUserContainer.animate()
        likeCommentContainer.animate()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func showLike(sender: AnyObject) {
        self.delegate?.showLikeView()
    }
    @IBAction func showComment(sender: AnyObject) {
        
        self.delegate?.showCommentView()
    }

}
