//
//  ProfileFeedCell.swift
//  Glance
//
//  Created by Professional on 2015-07-01.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class ProfileFeedCell: UITableViewCell {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var overlayLabel: UILabel!
    @IBOutlet var thumbnail:UIImageView!
    var thumbnailImage: UIImage?
    var videoModel:VideoFeedModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
