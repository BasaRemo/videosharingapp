
//
//  SearchCell.swift
//  Glance
//
//  Created by Professional on 2015-06-29.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

     @IBOutlet weak var cityLabel: UILabel!
     @IBOutlet weak var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // initialize the date formatter only once, using a static computed property
    static var dateFormatter: NSDateFormatter = {
        var formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
        }()
    
    var city: CityModel? {
        didSet {
            if let city = city, cityLabel = cityLabel, countryLabel = countryLabel {
                self.cityLabel.text = city.name
                self.countryLabel.text = city.country_code
            }
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
