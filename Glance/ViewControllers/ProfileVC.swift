//
//  ProfileVC.swift
//  Glance
//
//  Created by Professional on 2015-07-01.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MediaPlayer
import Foundation
import SABlurImageView
import Haneke

let profileViewVideoTransition = ProfileViewVideoTransition()
let profileViewVideoDismiss = ProfileViewVideoDismiss()

class ProfileVC: UIViewController,APIDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var profileInfoView: UIView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var interests: UILabel!
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var followingNumber: UILabel!
    @IBOutlet weak var followersNumber: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    var backgroundImageView:SABlurImageView?
    var animator:ZFModalTransitionAnimator!
    
    var feedsList = [VideoFeedModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        profileImageView.layer.borderColor = UIColor.whiteColor().CGColor
        profileImageView.layer.borderWidth = 4.0;
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
        profileImageView.layer.masksToBounds = true;
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        // Fetch user videos
        GlanceAPISingleton.fetchUserVideos(){ (error, result) -> () in
            self.didReceiveFeeds(result!)
            self.tableView.reloadData()
        }
        
        self.tabBarController?.tabBar.hidden = false
        if self.backgroundImageView != nil {
            self.backgroundImageView!.removeFromSuperview()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        self.applyBlurEffect2()
    }
    
    func applyBlurEffect2(){
        
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.profileInfoView.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.profileInfoView.bounds
            self.profileInfoView.insertSubview(blurEffectView, belowSubview: self.profileInfoView.viewWithTag(10)!) //if you have more UIViews on screen, use insertSubview:belowSubview: to place it underneath the lowest view instead
            
        } else {
            self.view.backgroundColor = UIColor.blackColor()
        }
    }
    
    func applyBlurEffect(){
        
        UIGraphicsBeginImageContextWithOptions(UIScreen.mainScreen().bounds.size, false, 0);
        self.view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        var image:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        backgroundImageView = SABlurImageView(image: image)
        backgroundImageView!.configrationForBlurAnimation()
        backgroundImageView?.blur(0.9)
        
        self.view.addSubview(backgroundImageView!)
    }
    
    @IBAction func settingsBtnTapped(sender: UIButton) {
        
        let modalVC = storyboard!.instantiateViewControllerWithIdentifier("SettingsVC") as? SettingsVC
        
        self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.5
        self.animator.behindViewScale = 1.0
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Left
        self.animator.setContentScrollView(modalVC!.tableView)
        
        modalVC!.transitioningDelegate = self.animator
        modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(modalVC!, animated: true, completion: nil)
    }
    
    func didReceiveFeeds(result: SwiftyJSON.JSON){
        
        //println(result)
        //Test
        /*if let userName = result["objects"][0]["video"].string{
        println(userName)
        }*/
        
        var feedsArray = [VideoFeedModel]()
        
        //println(result["objects"])
        
        for (index: String, jsonFeed: SwiftyJSON.JSON) in result["objects"] {
            println(jsonFeed["thumbnail"])
            
            let stringURL = jsonFeed["thumbnail"].string
            
            var feedModel = VideoFeedModel(
                video: jsonFeed["video"].string!,
                thumbnailUrl:stringURL!
            )
            feedModel.downloadImage()
            println(feedModel.video)
            feedsArray.append(feedModel)
        }
        // Set our list of new models
        feedsList = feedsArray
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


//Mark - Transitions animation
extension ProfileVC: UIViewControllerTransitioningDelegate {
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return profileViewVideoTransition
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return profileViewVideoDismiss
    }
    
}

extension ProfileVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = cell as? ProfileFeedCell
        
        if feedsList.count > 0{
            
            var video:VideoFeedModel = (feedsList[indexPath.row] as? VideoFeedModel)!
            cell!.videoModel = video
            var imageView:UIImageView = cell!.contentView.viewWithTag(13) as! UIImageView
            let URL = NSURL(string: video.thumbnailUrl!)
            imageView.hnk_setImageFromURL(URL!)
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var videoModel = feedsList[indexPath.row] as? VideoFeedModel
        
        if let videoPlayerVC = storyboard!.instantiateViewControllerWithIdentifier("videoPlayer") as? VideoPlayerVC {
            
            videoPlayerVC.transitioningDelegate = self
            if feedsList.count > 0{
                var videoModel = feedsList[indexPath.row] as? VideoFeedModel
                
                videoPlayerVC.videoPath = videoModel!.video
                videoPlayerVC.playVideo()
                presentViewController(videoPlayerVC, animated: true, completion: nil)
            }
        
        }
        
    }
    
}

extension ProfileVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 1
        return feedsList.count > 0 ? feedsList.count: 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("ProfileFeedCell") as? ProfileFeedCell {
            
            if feedsList.count > 0{
                
                //(cell.contentView.viewWithTag(11) as UILabel).text = LABELS[indexPath.row]
                cell.overlayLabel.hidden = false
                
            }
        
            return cell
        } else {
            NSLog("Prototype did not work")
            let cell = tableView.dequeueReusableCellWithIdentifier("ProfileFeedCell") as? ProfileFeedCell
            cell?.textLabel?.text = "No Videos"
            return cell!
        }
    }
    
}

