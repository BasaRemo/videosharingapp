//
//  LoginVC.swift
//  Glance
//
//  Created by Professional on 2015-06-10.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import SABlurImageView
import Alamofire

class LoginVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var backgroundImage: SABlurImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyBlurEffect()
    }
    func applyBlurEffect(){
        backgroundImage.configrationForBlurAnimation()
        //backgroundImage.startBlurAnimation(duration: 0.0)
        //backgroundImage.addBlurEffect(100, times: 1)
        backgroundImage?.blur(0.9)
    }
    override func viewWillAppear(animated: Bool)
    {
    }

    @IBAction func backBtnTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func loginBtnTapped(sender: AnyObject) {
        
        let user = (self.view.viewWithTag(10) as! UITextField).text
        let password = (self.view.viewWithTag(11) as! UITextField).text
        
        
        if self.validateLoginInfo(user, passwd: password){
            
            //GlanceAPISingleton.login(user, password: password)
            self.showFeedsView()
            /*Alamofire.request(.GET, "\(LOGIN_URL)/\(user)/\(password)")
                .authenticate(usingCredential: credential)
                .response {(request, response, _, error) in
                    if error == nil {
                        //println(response)
                        self.showFeedsView()
                    }else{
                        println(error)
                    }
            }*/
        }
        
    }
    
    func showFeedsView(){
        
        if let tabViewController = storyboard!.instantiateViewControllerWithIdentifier("tabBar") as? CustomTabBarVC {
            presentViewController(tabViewController, animated: true, completion: nil)
        }

    }
    // LOGIN VALIDATION
    func validateLoginInfo(userName:  String, passwd: String) -> Bool{
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        //move textfields up
        let myScreenRect: CGRect = UIScreen.mainScreen().bounds
        let keyboardHeight : CGFloat = 216
        
        UIView.beginAnimations( "animateView", context: nil)
        var movementDuration:NSTimeInterval = 0.5
        var needToMove: CGFloat = 0
        
        var frame : CGRect = self.view.frame
        if (textField.frame.origin.y + textField.frame.size.height + self.navigationController!.navigationBar.frame.size.height + UIApplication.sharedApplication().statusBarFrame.size.height > (myScreenRect.size.height - keyboardHeight)) {
            needToMove = (textField.frame.origin.y + textField.frame.size.height + self.navigationController!.navigationBar.frame.size.height + UIApplication.sharedApplication().statusBarFrame.size.height) - (myScreenRect.size.height - keyboardHeight);
        }
        
        frame.origin.y = -needToMove
        self.view.frame = frame
        UIView.commitAnimations()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        //move textfields back down
        UIView.beginAnimations( "animateView", context: nil)
        var movementDuration:NSTimeInterval = 0.35
        var frame : CGRect = self.view.frame
        frame.origin.y = 0
        self.view.frame = frame
        UIView.commitAnimations()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }

}
