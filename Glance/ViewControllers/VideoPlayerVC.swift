//
//  VideoPlayerVC.swift
//  Glance
//
//  Created by Professional on 2015-06-09.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import MediaPlayer
import Foundation
import SCRecorder
import Haneke
import UIImageViewModeScaleAspect
//import MHGallery
//import MHGalleryImageViewerViewController


let ViewControllerVideoPath = "https://d2hsyytjp5izl3.cloudfront.net/videos/shutterstock_v5454419.m3u8"

class VideoPlayerVC: UIViewController {

    var moviePlayer : MPMoviePlayerController!
    var videoPath: String?
    var recordSession: SCRecordSession!
    var thumbnail:UIImage?
    var imageThumb :UIImageViewModeScaleAspect?
    var counter = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //playRecorderVideo()
        
        var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGestureRecognizer:")
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "handleSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "videoEnded:", name: MPMoviePlayerPlaybackDidFinishNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "videoCanPlay:", name: MPMoviePlayerLoadStateDidChangeNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "videoPlayerViewControllerDidReceiveVideo:", name: MPMoviePlayerLoadStateDidChangeNotification, object: nil)
    
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillAppear(animated: Bool)
    {
        /*self.imageThumb = UIImageView(image: thumbnail)
        self.imageThumb!.frame = self.view.frame
        self.view.addSubview(self.imageThumb!)*/
    }
    
    func videoPlayerViewControllerDidReceiveVideo(notification: NSNotification){
        if self.counter < 1 {
            println("uno PLay")
            counter++
        }else{
            println("Ready PLay")
            self.imageThumb?.removeFromSuperview()
        }
        
        //videoString = notification.userInfo!.description as NSString
    }
    
    func videoEnded(notification: NSNotification){
        
        let userInfo = notification.userInfo as! [String:NSNumber]
        let reason = userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey]
        let finishReason = MPMovieFinishReason(rawValue: reason!.integerValue)
    }
    
    func videoCanPlay(notification: NSNotification){
        println("Can PLay")
        /*var moviePlayerController = notification.object
            as! MPMoviePlayerController
        var loadState: NSMutableString
        var state = moviePlayerController.loadState as MPMovieLoadState*/

    }
    
    func playMediaPlayer() {
        
        //println("videoPath: \(videoPath)")
        var url:NSURL = NSURL(string: videoPath!)!
        
        moviePlayer = MPMoviePlayerController(contentURL: url)
        self.moviePlayer.view.frame = CGRectMake(0 , 0, self.view.frame.width, self.view.frame.height)
        self.view.addSubview(moviePlayer.view)
        moviePlayer.fullscreen = true
        moviePlayer.controlStyle = MPMovieControlStyle.None
        moviePlayer.play()
        
    }
    
    func playVideo() {
        
        let URL = NSURL(string: videoPath!)!
        //let path = NSBundle.mainBundle().pathForResource("video", ofType:"mp4")
        //let url = NSURL.fileURLWithPath(path!)
        
        //cache.fetch(key:videoPath!).onSuccess { data in
            // Do something with data
            self.moviePlayer = MPMoviePlayerController(contentURL: URL)
            self.moviePlayer.view.frame = CGRectMake(0 , 0, self.view.frame.width, self.view.frame.height)
            self.view.addSubview(self.moviePlayer.view)
            self.moviePlayer.fullscreen = true
            self.moviePlayer.controlStyle = MPMovieControlStyle.None
            /*println("Succes fetching data!")
            let videoData:NSData? = data
            self.playVideoWithData(videoData!)*/
        
       // }
        
        
        /*
        
        */
    }
    
    func playVideoWithData(data:NSData){
        
        
        let videoData:NSData? = data
        
        //let plainString = "foo";
        //let plainData = (plainString as NSString).dataUsingEncoding(NSUTF8StringEncoding)

        
        //var avatar64 = videoData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //let base64String = videoData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //let datastring = NSString(data: base64String, encoding:NSUTF8StringEncoding)
        let decodedString = NSString(data: data, encoding: NSUTF8StringEncoding)
        //let cString = CString(videoData?.bytes)
        //let string = String.fromCString(cString)
        //println("\(videoData)")
        //let datastring = NSString(data: videoData!, encoding:NSASCIIStringEncoding)
        //let datastring = NSString(data: videoData!, encoding: NSUTF8StringEncoding) as? String ?? ""
         //println("DATA: \(base64String)")
        //var videoUrl:NSURL = NSURL(fileURLWithPath: "\(datastring)")!
        //var url:NSURL = NSURL(string: "\(base64String)")!*/
        
       // moviePlayer = MPMoviePlayerController(contentURL: url)
        self.moviePlayer.view.frame = CGRectMake(0 , 0, self.view.frame.width, self.view.frame.height)
        self.view.addSubview(self.moviePlayer.view)
        self.moviePlayer.fullscreen = true
        self.moviePlayer.controlStyle = MPMovieControlStyle.None
        
        //let videoUrl = NSURL(fileURLWithPath: videoPath)
        
        let URL = NSURL(string: videoPath!)!
        let fetcher = NetworkFetcher<NSData>(URL: URL)
        
        cache.fetch(fetcher: fetcher).onSuccess { video in
            
            self.moviePlayer = MPMoviePlayerController(contentURL: URL)
            self.moviePlayer.view.frame = CGRectMake(0 , 0, self.view.frame.width, self.view.frame.height)
            self.view.addSubview(self.moviePlayer.view)
            self.moviePlayer.fullscreen = true
            self.moviePlayer.controlStyle = MPMovieControlStyle.None
            
            println("video Player")
        }
        
    }

    func playRecorderVideo(){
        
        var player = SCPlayer()
        player.setItemByAsset(recordSession?.assetRepresentingSegments())
        
        var playerView = SCVideoPlayerView(player: player)
        playerView.frame = self.view.frame

        //var playerView = UIView(frame: self.view.frame)
        playerView.backgroundColor = UIColor.blackColor()
        self.view.addSubview(playerView)
        
        /*let playerLayer = AVPlayerLayer()
        playerLayer.frame = playerView.bounds
        playerLayer.player = player
        playerView.layer.addSublayer(playerLayer)*/
        
        
        player.play()
        
        // Render the video directly through a filter
        playerView.SCImageViewEnabled = true;
        var blackAndWhite:SCFilter = SCFilter(CIFilterName: "CIPhotoEffectInstant")
        playerView.SCImageView.filter = blackAndWhite
        
        var dur:Float64 = CMTimeGetSeconds(self.recordSession.duration);
        var durInMiliSec:Float64 = 1000*dur;
        println("duration: \(durInMiliSec/1000)")
    }
    
    // MARK: UIGestureRecognizer
    
    func handleTapGestureRecognizer(gestureRecognizer: UITapGestureRecognizer) {
        //self.dismissViewControllerAnimated(true, completion: nil)
        self.moviePlayer.play()
    }
    func handleSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                println("Swiped right")
            case UISwipeGestureRecognizerDirection.Down:
                println("Swiped down")
                self.dismissViewControllerAnimated(true, completion: nil)
            default:
                break
            }
        }
    }
}
