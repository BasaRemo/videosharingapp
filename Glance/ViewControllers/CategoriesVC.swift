//
//  CategoriesVC.swift
//  Glance
//
//  Created by Professional on 2015-06-17.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
protocol CategoryDelegate {
    func categorySelected(controller: CategoriesVC,selectedCategory:String?)
    func showHiddenElements(controller: CategoriesVC)
}

class CategoriesVC: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{

    @IBOutlet weak var categoryCollection: UICollectionView!
    //delegate variable
    var delegate: CategoryDelegate?
    var icons = [
        "category_news_nor","category_events_nor","category_travel_nor",
         "category_sports_nor","category_music_nor","category_arts_nor",
        "category_nightlife_nor","category_outings_nor","category_fashion_nor"
    ]
    var iconsLabels = [
        "News","Events","Travel",
        "Sports","Music","Arts",
        "Nightlife","Outlings","Fashion"
    ]
    var allowMultipleSelection:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.categoryCollection.dataSource = self
        self.categoryCollection.delegate = self
        
        var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGestureRecognizer:")
        tapGestureRecognizer.numberOfTapsRequired = 1
        //self.view.addGestureRecognizer(tapGestureRecognizer)
        
    }
    override func viewWillAppear(animated: Bool) {
        
        self.applyBlurEffect()
    }
    
    @IBAction func closeView(sender: UIButton) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        //self.performSegueWithIdentifier("showVideoPlayer", sender: self)
        //self.uploadVideo()
        
    }
    
    func applyBlurEffect(){
        
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.categoryCollection.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.categoryCollection.bounds

            // Insert blured View on Collection VIew
            self.categoryCollection.insertSubview(blurEffectView, belowSubview: self.categoryCollection.viewWithTag(10)!)
            
            //Add Constraints
            blurEffectView.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            let horizontalConstraint = NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.categoryCollection, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
            self.categoryCollection.addConstraint(horizontalConstraint)

            let verticalConstraint = NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.categoryCollection, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
            self.categoryCollection.addConstraint(verticalConstraint)
//
            let widthConstraint = NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 500)
            blurEffectView.addConstraint(widthConstraint)
            // view.addConstraint(widthConstraint) // also works

            let heightConstraint = NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 500)
            blurEffectView.addConstraint(heightConstraint)
            
        } else {
            self.categoryCollection.backgroundColor = UIColor.blackColor()
        }
    }
    
    func showPopUp(parentView: UIView!)
    {
        //parentView.addSubview(self.view)
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func closePopUp()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    //Mark - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let headerView = self.categoryCollection?.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "header", forIndexPath: indexPath) as! UICollectionReusableView
        return headerView
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return icons.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath) as! UICollectionViewCell
        
        var imageView: UIImageView = (cell.contentView.viewWithTag(10) as! UIImageView);
        imageView.image = UIImage(named:icons[indexPath.row])
        //(cell.contentView.viewWithTag(11) as! UILabel).text = iconsLabels[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView,
        didSelectItemAtIndexPath indexPath: NSIndexPath) {
            println("\(indexPath.row)")
            self.delegate?.showHiddenElements(self)
            self.delegate?.categorySelected(self, selectedCategory: iconsLabels[indexPath.row])
            //if self.categoryCollection.is
            if !allowMultipleSelection{
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            
    }
    
    func handleTapGestureRecognizer(gestureRecognizer: UITapGestureRecognizer) {
        
        //self.delegate?.showHiddenElements(self)
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
