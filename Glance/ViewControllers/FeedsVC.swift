//
//  FeedsVC.swift
//  Glance
//
//  Created by Professional on 2015-06-09.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MediaPlayer
import Foundation
import SABlurImageView
import Haneke
import ASMediaFocusManager

let customPresentTransitionAC = CustomPresentTransitionAC()
let customDismissTransitionAC = CustomDismissTransitionAC()

class FeedsVC: UIViewController {
    @IBOutlet weak var playerContainer: UIView!
    
    var dataSourceSet:Bool = false
    //var feedsList: NSMutableArray = []
    var feedsList = [VideoFeedModel]()
    var moviePlayer : MPMoviePlayerController!
    var animator:ZFModalTransitionAnimator!
    var backgroundImageView:SABlurImageView?
    var mediaFocusManager:ASMediaFocusManager?
    
    @IBOutlet weak var feedsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.feedsTableView.dataSource = self
        self.feedsTableView.delegate = self
        //UITabBar.appearance().barTintColor = UIColor.lightGrayColor()
        UITabBar.appearance().tintColor = THEME_COLOR
        UIApplication.sharedApplication().statusBarHidden = false
         //self.tabBarController?.selectedIndex = 0
        
        var tabBarItem : UITabBarItem = self.tabBarController?.tabBar.items![1] as! UITabBarItem
        tabBarItem.image = UIImage(named:"record_tab")!.imageWithRenderingMode(.AlwaysOriginal)
        

    }
    func applyBlurEffect(){
        
        UIGraphicsBeginImageContextWithOptions(UIScreen.mainScreen().bounds.size, false, 0);
        self.view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        var image:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        //Crop the captured Image to the commentView Size
        var frame = CGRectMake(0, 171, 320, 397)
        var imageRef:CGImageRef = CGImageCreateWithImageInRect(image.CGImage, frame);
        var croppedImage:UIImage = UIImage(CGImage: imageRef)!
        
        //Blur the imageview with size of commentView and add it to this view
        backgroundImageView = SABlurImageView(image: image)
        backgroundImageView?.frame = frame
        backgroundImageView!.configrationForBlurAnimation()
        backgroundImageView?.blur(0.95)
        
        self.view.addSubview(backgroundImageView!)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        // Fetch user videos
        GlanceAPISingleton.loadFeedsVideos(){ (error, result) -> () in
            self.didReceiveFeeds(result!)
            self.feedsTableView.reloadData()
        }
        
        self.tabBarController?.tabBar.hidden = false
        if self.backgroundImageView != nil {
            self.backgroundImageView!.removeFromSuperview()
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    
    }
    @IBAction func segmentedControlChanged(sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        default:
            break;
        } 
    }

    func didReceiveFeeds(result: SwiftyJSON.JSON){
        
        /*if !dataSourceSet{
            self.feedsTableView.dataSource = self
            dataSourceSet = true
        }*/
        
        var feedsArray = [VideoFeedModel]()
        //println(result["objects"])
        for (index: String, jsonFeed: SwiftyJSON.JSON) in result["objects"] {
            println(jsonFeed["thumbnail"])
            
            let stringURL = jsonFeed["thumbnail"].string
            var feedModel = VideoFeedModel(
                video: jsonFeed["video"].string!,
                thumbnailUrl:stringURL!
            )
            feedModel.downloadImage()
            feedsArray.append(feedModel)
        }
        // Set our list of new models
        feedsList = feedsArray
        self.feedsTableView.reloadData()
        
    }

    @IBAction func selectCategories(sender: UIButton) {
        
        if let categoryVC = storyboard!.instantiateViewControllerWithIdentifier("categoryVC") as? CategoriesVC {
            //categoryVC.transitioningDelegate = self
            
//            let snapshotView = UIView()
//            //fromViewController.view.snapshotViewAfterScreenUpdates(false)
//            snapshotView.backgroundColor = UIColor(patternImage: UIImage(named: "sign_bkg@2x.png")!)
//            snapshotView.frame = CGRectMake(0 , 100, fromViewController.view.frame.width, 321)
//            self.view.addSubview(snapshotView)
            
            let window: UIWindow! = UIApplication.sharedApplication().keyWindow
            let windowImage = window.capture()
            
            let blurImageView:SABlurImageView = SABlurImageView(image: windowImage)
            blurImageView.configrationForBlurAnimation()
            blurImageView.blur(0.95)
            blurImageView.frame = categoryVC.view.frame
            categoryVC.view.insertSubview(blurImageView, belowSubview: categoryVC.categoryCollection)
            
            categoryVC.delegate = self
            categoryVC.allowMultipleSelection = true
            //categoryVC.view.backgroundColor = UIColor(patternImage: windowImage)
            categoryVC.categoryCollection.allowsMultipleSelection = true
            presentViewController(categoryVC, animated: false, completion: completionHandler)
        }
    }
    func completionHandler() {

    }
    
    @IBAction func goToSearchView(sender: UIButton) {

        //self.applyBlurEffect()
        
        let modalVC = storyboard!.instantiateViewControllerWithIdentifier("searchVC") as? SearchVC
        
        self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.5
        self.animator.behindViewScale = 1.0
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Left
        self.animator.setContentScrollView(modalVC!.tableView)
        
        modalVC!.transitioningDelegate = self.animator
        modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(modalVC!, animated: true, completion: nil)
    }
    
    func goToCommentView() {
        
        let modalVC = storyboard!.instantiateViewControllerWithIdentifier("commentVC") as? CommentVC
        
        self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.5
        self.animator.behindViewScale = 1.0
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Bottom
        self.animator.setContentScrollView(modalVC!.tableView)
        
        modalVC!.transitioningDelegate = self.animator
        modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(modalVC!, animated: true, completion: nil)
    }
    
    func goToLikeView() {
        
        let modalVC = storyboard!.instantiateViewControllerWithIdentifier("likeVC") as? LikeVC
        
        self.animator = ZFModalTransitionAnimator(modalViewController: modalVC)
        self.animator.dragable = true
        self.animator.bounces = false
        self.animator.behindViewAlpha = 0.5
        self.animator.behindViewScale = 1.0
        self.animator.transitionDuration = 0.7
        self.animator.direction = ZFModalTransitonDirection.Bottom
        self.animator.setContentScrollView(modalVC!.tableView)
        
        modalVC!.transitioningDelegate = self.animator
        modalVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(modalVC!, animated: true, completion: nil)
    }
    
    func playVideo(videoPath:String){
        
        var url:NSURL = NSURL(string: videoPath)!
        
        moviePlayer = MPMoviePlayerController(contentURL: url)
        self.moviePlayer.view.frame = CGRectMake(0 , 0, self.view.frame.width, self.view.frame.height)
        self.view.addSubview(moviePlayer.view)
        moviePlayer.fullscreen = true
        /* Capture the frame at the third second into the movie */

        
        moviePlayer.controlStyle = MPMovieControlStyle.None
        //moviePlayer.play()
    }
    
    func checkCellCompleteVisibility(){
        
        var visibleCells = [NSIndexPath]()
        visibleCells = feedsTableView.indexPathsForVisibleRows() as! [NSIndexPath]
        
        for indexPath in visibleCells{
            
            var cellRect:CGRect = feedsTableView.rectForRowAtIndexPath(indexPath)
            var completelyVisible = CGRectContainsRect(feedsTableView.bounds, cellRect)
            var currentCell:FeedCell = feedsTableView.cellForRowAtIndexPath(indexPath) as! FeedCell
            if completelyVisible{
                currentCell.overlayLabel.hidden = false
            }else{
                currentCell.overlayLabel.hidden = true
            }
        }
    }
}

extension FeedsVC: CategoryDelegate {
    
    func showCommentView(){
        self.goToCommentView()
    }
    func showLikeView(){
        self.goToLikeView()
    }
}

extension FeedsVC: FeedCellDelegate {
    
    func categorySelected(controller: CategoriesVC,selectedCategory:String?){
        
        
    }
    func showHiddenElements(controller: CategoriesVC){
        println("Here")
    }
}

//Mark - Transitions animation
extension FeedsVC: UIViewControllerTransitioningDelegate {
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customPresentTransitionAC
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customDismissTransitionAC
    }
    
    // MARK: - Navigation
    /*override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
    if segue.identifier == "showVideo" {
    let toViewController = segue.destinationViewController as! VideoPlayerVC
    toViewController.transitioningDelegate = self
    }
    }*/
}
//Mark - UITableView Delegate
extension FeedsVC: UITableViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.checkCellCompleteVisibility()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        //self.goToCommentView()
        var videoModel = feedsList[indexPath.row] as? VideoFeedModel
        
        if let videoPlayerVC = storyboard!.instantiateViewControllerWithIdentifier("videoPlayer") as? VideoPlayerVC {
            videoPlayerVC.transitioningDelegate = self
            
            if feedsList.count > 0{
                var videoModel = feedsList[indexPath.row] as? VideoFeedModel
                //videoPlayerVC.videoPath = "https://glanceapp.s3.amazonaws.com/videos/videoTest_468df32ffed8d474abe182cc48d4d8a2.mp4"
                videoPlayerVC.videoPath = videoModel!.video
                videoPlayerVC.thumbnail = videoModel?.image
                videoPlayerVC.playVideo()
                presentViewController(videoPlayerVC, animated: true, completion: nil)
            }else{
                let alert = UIAlertView()
                alert.title = "Alert"
                alert.message = "You have no videos, create one"
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
            
        }
        
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
       //let cell = tableView.dequeueReusableCellWithIdentifier("feedCell") as? FeedCell
        let cell = cell as? FeedCell
        /*var video:VideoFeedModel = feedsList[indexPath.row] as! VideoFeedModel

        if let thumbnailImg = video.image {
            println("ICI")
            cell!.thumbnailImage = thumbnailImg
        }*/
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("feedCell") as? FeedCell
        cell?.overlayLabel.hidden = true
    }
}

extension FeedsVC: UITableViewDataSource {
    
    //Mark - UITableView DataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return feedsList.count > 0 ? feedsList.count: 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("feedCell") as? FeedCell {
            cell.delegate = self
            
            let video:VideoFeedModel = feedsList[indexPath.row] as VideoFeedModel
            cell.videoModel = video
            if let thumbnailImg = video.image {
                println("ICI")
                cell.thumbnailImage = thumbnailImg
            }
            //var imageView:UIImageView = cell.contentView.viewWithTag(13) as! UIImageView
            let URL = NSURL(string: video.thumbnailUrl!)
            cell.thumbnail.hnk_setImageFromURL(URL!)
            
            //(cell.contentView.viewWithTag(11) as UILabel).text = LABELS[indexPath.row]
            return cell
        } else {
            NSLog("Prototype did not work")
            return UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "errorIdentifier")
        }
    }
    
}

extension FeedsVC: ASMediasFocusDelegate {
    
    func parentViewControllerForMediaFocusManager(mediaFocusManager: ASMediaFocusManager!) -> UIViewController! {
        return self
    }
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, mediaURLForView view: UIView!) -> NSURL! {
        let video:VideoFeedModel = feedsList[2] as VideoFeedModel
        let URL = NSURL(string: video.thumbnailUrl!)
        return URL
    }
    func mediaFocusManagerWillAppear(mediaFocusManager: ASMediaFocusManager!) {
        
    }
    func mediaFocusManagerWillDisappear(mediaFocusManager: ASMediaFocusManager!) {
        
    }
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, titleForView view: UIView!) -> String! {
        return ""
    }
    /*func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, finalFrameForView view: UIView!) -> CGRect {
        <#code#>
    }
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, cachedImageForView view: UIView!) -> UIImage! {
        <#code#>
    }
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, imageViewForView view: UIView!) -> UIImageView! {
        <#code#>
    }*/

}
