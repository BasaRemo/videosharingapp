//
//  SearchVC.swift
//  Glance
//
//  Created by Professional on 2015-06-28.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift

class SearchVC: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var city = ["San Francisco","New York","San Jose","Chicago","Los Angeles","Austin","Seattle"]
    var tag = ["Fun","Love","Easy","Money","instagram","sexy","data"]
    var user = ["Alain","Bronson","Caraine","Jon","Luke","Martin","Alex","Adrian"]
    var searchResults = [CityModel]()
    var data = [CityModel]()
    var currentData: [String] = []
    var currentSearchResult :[String] = []
    var endPoint:String = "city"
    var searchActive : Bool = false
    
    enum State {
        case DefaultMode
        case SearchMode
    }
    enum SearchMode {
        case City
        case Tag
        case User
    }
    
    var state: State = .DefaultMode {
        
        didSet {
            // update notes and search bar whenever State changes
            switch (state) {
            case .DefaultMode:
                //let realm = Realm()
                //notes = realm.objects(Note).sorted("modificationDate", ascending: false) //1
                //self.navigationController!.setNavigationBarHidden(false, animated: true) //2
                searchBar.resignFirstResponder() // 3
                searchBar.text = ""
                searchBar.showsCancelButton = false
            case .SearchMode:
                let searchText = searchBar?.text ?? ""
                searchBar.setShowsCancelButton(true, animated: true) //4
                //notes = searchNotes(searchText) //5
                //self.navigationController!.setNavigationBarHidden(true, animated: true) //6
            }
        }
    }

    var cities: Results<CityModel>! {
        didSet {
            // Whenever notes update, update the table view
            tableView?.reloadData()
        }
    }
    
    var searchMode: SearchMode = .City {
        
        didSet {
            // update notes and search bar whenever State changes
            switch (searchMode) {
            case .City:
                endPoint = "City"
            case .Tag:
                endPoint = "tag"
            case .User:
                endPoint = "user"

            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        
        // Reload the table
        self.tableView.reloadData()
        self.view.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5)

        self.searchBar.backgroundImage = UIImage()
        self.searchBar.translucent = true
        self.searchBar.backgroundColor = UIColor.clearColor()
        self.currentData = self.city
         self.applyBlurEffect()
        
//        GlanceAPISingleton.searchCity() { (error, result) -> () in
//            //Do your stuff
//            self.didReceiveCity(result!)
//            self.tableView.reloadData()
//        }

    }

    override func viewWillAppear(animated: Bool) {
    
        super.viewWillAppear(animated)
        
//        let realm = Realm()
//        cities = realm.objects(CityModel)//.sorted("name", ascending: false)
//        state = .DefaultMode
    }
    
    func applyBlurEffect(){
        
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view.bounds
            self.view.insertSubview(blurEffectView, belowSubview: self.view.viewWithTag(10)!) //if you have more UIViews on screen, use insertSubview:belowSubview: to place it underneath the lowest view instead
            
        } else {
            self.view.backgroundColor = UIColor.blackColor()
        }
    }
    
//    func searchCities(searchString: String) -> Results<CityModel> {
//        let realm = Realm()
//        let searchPredicate = NSPredicate(format: "name CONTAINS[c] %@", searchString)
//        return realm.objects(CityModel).filter(searchPredicate)
//    }
    
//    func updateSearchResults(searchString: String) {
//        
//        searchResults.removeAll(keepCapacity: false)
//        
//        let searchPredicate = NSPredicate(format: "name CONTAINS[c] %@", searchString)
//        
//        //let array = (data as NSArray).filteredArrayUsingPredicate(searchPredicate)
//        
//        let array = data as [CityModel]
//        
//        searchResults = array.filter({
//            $0.name == searchString
//        })
//        self.tableView.reloadData()
//        
//       /* searchResults = array.filter { (city : CityModel) -> Bool in
//            return contains(city.name, searchPredicate)
//        }*/
//        
//        //searchResults = array as! [CityModel]
//        
//        // Fetch Cities
//        /*GlanceAPISingleton.searchCity (){ (error, result) -> () in
//            //Do your stuff
//            self.didReceiveCity(result!)
//            self.tableView.reloadData()
//        }*/
//    }
    
    @IBAction func indexChanged(sender: UISegmentedControl) {
        
        if(segmentedControl.selectedSegmentIndex == 0){
            searchResults.removeAll(keepCapacity: false)
            self.currentData = self.city
        } else if(segmentedControl.selectedSegmentIndex == 1){
            self.currentData = self.tag
            searchResults.removeAll(keepCapacity: false)
        } else if(segmentedControl.selectedSegmentIndex == 2){
            searchResults.removeAll(keepCapacity: false)
            self.currentData = self.user
        } else {
            searchResults.removeAll(keepCapacity: false)
        }
        self.searchActive = false
        self.tableView.reloadData()
        
    }
    
//    func didReceiveCity(result: SwiftyJSON.JSON){
//        
//        println("Received Data")
//        let realm = Realm()
//        //Test
//        /*if let userName = result["objects"][0]["video"].string{
//        println(userName)
//        }*/
//
//        var feedsArray = [CityModel]()
//
//        for (index: String, jsonFeed: JSON) in result["objects"] {
//            //println(jsonFeed["name"])
//            
//            /*let stringURL = jsonFeed["thumbnail"].string
//            let url = NSURL(string: stringURL!)
//            let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
//            var image = UIImage(data: data!)*/
//            
//            let dateFormatter = NSDateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSSxxx"
//            
//            //Parse date
//            let created_datetime = dateFormatter.dateFromString(jsonFeed["created_datetime"].string!) ?? NSDate()
//            let modified_datetime = dateFormatter.dateFromString(jsonFeed["modified_datetime"].string!) ?? NSDate()
//            
//            //Parse longitude and latitude
//            let separators = NSCharacterSet(charactersInString: " )(")
//            var locationFullString = jsonFeed["location"].string!
//            var locationString = String(Array(locationFullString)[6...locationFullString.length-1])
//            var words = locationString.componentsSeparatedByCharactersInSet(separators)
//            
//            var longitude:CGFloat = (CGFloat)((words[1] as NSString).floatValue)
//            var latitude:CGFloat = (CGFloat)((words[2] as NSString).floatValue)
//            
//            let location = CGPointMake(longitude, latitude)
//            
//            //Parse Population and region
//            var population = jsonFeed["population"].int ?? 0
//            var region = jsonFeed["region"].int ?? 0
//            
//            var cityModel = CityModel(
//                active: jsonFeed["active"].bool!,
//                pretty_name:jsonFeed["pretty_name"].string!,
//                resource_uri:jsonFeed["resource_uri"].string!,
//                country_code:jsonFeed["country_code"].string!,
//                name:jsonFeed["name"].string!,
//                id:jsonFeed["id"].int!,
//                population:population,
//                region:region,
//                created_datetime:modified_datetime,
//                modified_datetime:created_datetime//,
//                //location:location
//            )
//            
//            //println(cityModel.name)
//            realm.write() {
//                //cityModel.name = jsonFeed["pretty_name"].string!
//                //cityModel.country_code =  jsonFeed["country_code"].string!
//                realm.add(cityModel)
//            }
//            //feedsArray.append(cityModel)
//        }
//        // Set our list of new models
//        //data = feedsArray
//        println(realm.objects(CityModel))
//        //cities = realm.objects(CityModel)
//        self.tableView.reloadData()
//        
//    }
//    
    @IBAction func closeSearchView(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchActive{
            return Int(currentSearchResult.count ?? 0)
        }
        return Int(currentData.count ?? 0)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
//        //let city = cities[indexPath.row] as CityModel
////        cell.city = city

        var cell:SearchCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! SearchCell
        cell.cityLabel.hidden = true
        cell.countryLabel.hidden = true
        
        if searchActive{
            cell.textLabel?.text = self.currentSearchResult[indexPath.row] as? String
        }else{
            cell.textLabel?.text = self.currentData[indexPath.row] as? String
        }
        
        return cell

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
    }
}


extension SearchVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        state = .SearchMode
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        state = .DefaultMode
    }
    
//    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        //updateSearchResults(searchText)
//        cities = searchCities(searchText)
//    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        println("searchin..")
        self.currentSearchResult = self.currentData.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        if(currentSearchResult.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }

    
}

