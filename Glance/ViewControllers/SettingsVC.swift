//
//  SettingsVC.swift
//  Glance
//
//  Created by Professional on 2015-07-01.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var username = "Imed"
    var userCity = "Montreal,QC"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.view.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.5)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        self.applyBlurEffect()
    }
    
    func applyBlurEffect(){
        
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view.bounds
            self.view.insertSubview(blurEffectView, belowSubview: self.view.viewWithTag(10)!) //if you have more UIViews on screen, use insertSubview:belowSubview: to place it underneath the lowest view instead
            
        } else {
            self.view.backgroundColor = UIColor.blackColor()
        }
    }
    
    
    @IBAction func goBack(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingsVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        
        var view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 20))
        var label = UILabel(frame: CGRectMake(20,0, tableView.frame.size.width/2, 15))
        label.text="Profile"
        
        switch (section) {
        case 0:
            label.text = "Profile";
            break;
        case 1:
            label.text = "Account";
            break;
        case 2:
            label.text = "Social";
            break;
        default:
            break;
        }
        label.textColor = THEME_COLOR
        view.addSubview(label)
        
        return view
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = cell as? ProfileFeedCell

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    
}

extension SettingsVC: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("SettingsCell") as? SettingsCell{
        
            switch (indexPath.section) {
            case 0:
                
                var titles = ["Location", "Name", "Bio"]
                var details = ["\(userCity)", "\(username)", ""]
                
                cell.titleLabel.text = titles[indexPath.row]
                cell.detailsLabel.text = details[indexPath.row]
                
                switch (indexPath.row) {
                case 0:
                    let image = UIImage(named: "loading") as UIImage!
                    let playButton  = UIButton.buttonWithType(UIButtonType.System) as! UIButton
                    cell.icon!.setImage(image, forState: .Normal)
                    
                    break
                case 1:
                    break
                    
                default:
                    break
                }
                
                break;
                
            case 1:
                
                break
            case 2:
                break
            default:
                break
            }
            return cell
            
        }
        
        return UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "errorIdentifier")
    }
    
    
}
