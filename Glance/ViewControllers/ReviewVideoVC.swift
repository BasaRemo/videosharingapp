//
//  ReviewVideoVC.swift
//  Glance
//
//  Created by Professional on 2015-06-14.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import SCRecorder
import SABlurImageView
import MediaPlayer


let presentPopUpTransition = PresentPopUpTransition()
let dismissPopUpTransition = DismissPopUpTransition()

class ReviewVideoVC: UIViewController,UIViewControllerTransitioningDelegate,CategoryDelegate,UITextFieldDelegate {

    var moviePlayer : MPMoviePlayerController?
    @IBOutlet weak var itemsContainer: UIView!
    @IBOutlet weak var backgroundImage: SABlurImageView!
    var thumbnailImage:UIImage?
    var headline:String?
    var category:String?
    var recordSession: SCRecordSession?
    var categoryPopUp:CategoriesVC!
    var contentUrl:NSURL?
    var animator:ZFModalTransitionAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.getThumbnail()
        self.categoryPopUp = CategoriesVC()
        self.categoryPopUp?.delegate = self
        self.applyBlurEffect()
        
        var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "handleTapGestureRecognizer:")
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGestureRecognizer)
        //self.playVideo()
        
    }
    override func viewWillAppear(animated: Bool)
    {
        self.tabBarController?.tabBar.hidden = true
        
    }

    @IBAction func goToShareView(){
        
        if let viewController = storyboard!.instantiateViewControllerWithIdentifier("shareView") as? ShareVideoVC {
//            self.navigationItem.title = ""
//            navigationController?.pushViewController(shareVC, animated: true)
            
            self.animator = ZFModalTransitionAnimator(modalViewController: viewController)
            self.animator.dragable = true
            self.animator.bounces = false
            self.animator.behindViewAlpha = 1.0
            self.animator.behindViewScale = 1.0
            self.animator.transitionDuration = 0.7
            self.animator.direction = ZFModalTransitonDirection.Right
            //self.animator.setContentScrollView(viewController.tableView)
            
            viewController.transitioningDelegate = self.animator
            viewController.modalPresentationStyle = UIModalPresentationStyle.Custom
            presentViewController(viewController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func backToRecordView(){
        
        //navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func playVideo() ->Bool {
        
        //let path = NSBundle.mainBundle().pathForResource("xx", ofType:"mp4")
        let path = NSBundle.mainBundle().pathForResource("demo", ofType:"mp4")
        //take path of video
        
        let url = self.contentUrl
        
        
        moviePlayer = MPMoviePlayerController(contentURL: url)
        //asigning video to moviePlayer
        
        if let player = moviePlayer {
            player.view.frame = self.view.bounds
            //setting the video size to the view size
            
            player.controlStyle = MPMovieControlStyle.None
            //Hiding the Player controls
            
            
            player.prepareToPlay()
            //Playing the video
            
            
            player.repeatMode = .One
            //Repeating the video
            
            player.scalingMode = .AspectFill
            //setting the aspect ratio of the player
            
            self.view.addSubview(player.view)
            //adding the player view to viewcontroller
            return true
            
        }
        return false
    }
    
    func applyBlurEffect(){
        backgroundImage.configrationForBlurAnimation()
        //backgroundImage?.blur(1)
        backgroundImage.startBlurAnimation(duration: 0.1)
    }
    
    @IBAction func chooseCategory(sender: CustomButton) {
        
        if let categoryVC = storyboard!.instantiateViewControllerWithIdentifier("categoryVC") as? CategoriesVC {
            categoryVC.transitioningDelegate = self
            categoryVC.delegate = self
            itemsContainer.hidden = true
            presentViewController(categoryVC, animated: true, completion: nil)
        }
        //categoryPopUp!.showPopUp(self.view)
        //self.view.addSubview(categoryPopUp.view!)
    }
    func categorySelected(controller: CategoriesVC,selectedCategory:String?){
        
        var categoryLabel:UILabel =  (self.itemsContainer.viewWithTag(10) as! UILabel)
        categoryLabel.text = selectedCategory
        
    }
    func showHiddenElements(controller: CategoriesVC){
        println("Here")
        itemsContainer.hidden = false
    }
    func getThumbnail(){

        // Get some information about a particular segment
        if let segment:SCRecordSessionSegment = recordSession!.segments.first as? SCRecordSessionSegment{
            
            // Get thumbnail of this segment
            thumbnailImage = segment.thumbnail;
            //set background image
            self.view.backgroundColor = UIColor(patternImage: thumbnailImage!)
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        self.headline = textField.text
        if let hashtags = headline?.getHashtags(){
            for hashtag in hashtags{
                println("#\(hashtag)")
            }
        }
        if let mentions = headline?.getMentions(){
            for mention in mentions{
                println("@\(mention)")
            }
        }

        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    func handleTapGestureRecognizer(gestureRecognizer: UITapGestureRecognizer) {
        
        view.endEditing(true)
    }
    
    //Mark - Custom modal Transitions animation
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presentPopUpTransition
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissPopUpTransition
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
