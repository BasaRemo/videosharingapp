//
//  VideoRecorderVC.swift
//  Glance
//
//  Created by Professional on 2015-06-12.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import AVFoundation
import SCRecorder
import Alamofire
import AFNetworking
import SwiftyJSON
let customNavAnimationController = SignInUpPresentTransition()
class VideoRecorderVC: UIViewController,UINavigationControllerDelegate,SCRecorderDelegate {

    @IBOutlet weak var btnContainer: UIView!
    var recorder: SCRecorder!
    var photo: UIImage?
    var recordSession: SCRecordSession?
    var videoUrl:NSURL?
    var animator:ZFModalTransitionAnimator!
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet var previewView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        self.configureCamera()
        self.setProgressView()
    }
    override func viewWillAppear(animated: Bool)
    {
        self.tabBarController?.tabBar.hidden = true
        self.navigationController?.navigationBarHidden = true
        self.navigationItem.title = ""
    }
    
    func setProgressView(){
        
        //self.progressBar.progressTintColor = THEME_COLOR;
        self.progressBar.layer.cornerRadius = 5.0
        self.progressBar.layer.borderWidth = 2.0
        self.progressBar.layer.borderColor = UIColor.whiteColor().CGColor
        self.progressBar.trackTintColor = UIColor.clearColor()
        self.progressBar.layer.masksToBounds = true
        self.progressBar.clipsToBounds = true
        //self.progressBar.setProgress(0.0, animated: false)
        
        //var transform:CGAffineTransform = CGAffineTransformMakeScale(1.0, 10.0)
        //self.progressBar.transform = transform;
    }
    
    func configureCamera(){
        
        // Create the recorder
        self.recorder = SCRecorder()
        
        // Start running the flow of buffers
        if (!self.recorder.startRunning()) {
            println("Something wrong there: \(recorder.error)")
        }
        
        // Create a new session and set it to the recorder
        recordSession = SCRecordSession()
        self.recorder.session = recordSession;
        
        // Set the AVCaptureSessionPreset for the underlying AVCaptureSession.
        recorder.captureSessionPreset = AVCaptureSessionPresetHigh;
        
        // Set the video device to use
        recorder.autoSetVideoOrientation = true
        recorder.device = AVCaptureDevicePosition.Back;
        
        // Set the maximum record duration
        recorder.maxRecordDuration = CMTimeMake(10, 1);
        
        // Listen to the messages SCRecorder can send
        self.recorder.delegate = self;
        
        var previewView = self.previewView;
        recorder.previewView = previewView;
        
        var video:SCVideoConfiguration = recorder.videoConfiguration;
        
        // Whether the video should be enabled or not
        video.enabled = true;
        // The bitrate of the video video
        video.bitrate = 12000000; // 5Mbit/s
        // Size of the video output
        //video.size = CGSizeMake(1334, 750);
        // Scaling if the output aspect ratio is different than the output one
        //video.scalingMode = AVVideoScalingModeFit;
        // The timescale ratio to use. Higher than 1 makes a slow motion, between 0 and 1 makes a timelapse effect
        //video.timeScale = 1;
        // Whether the output video size should be infered so it creates a square video
        //video.sizeAsSquare = false;
        
        
    }
    
    @IBAction func switchCam(sender: AnyObject) {
        if recorder.device == AVCaptureDevicePosition.Back{
            recorder.device = AVCaptureDevicePosition.Front
        }else{
            recorder.device = AVCaptureDevicePosition.Back
        }
        
    }

    @IBAction func stopRecording(sender: AnyObject) {
        self.recorder.pause()
    }

    @IBAction func startRecording(sender: UIButton) {
        println("Record Start")
        self.recorder.record()
    }
    @IBAction func changeFlashState(sender: AnyObject) {
        self.recorder.pause()
        if recorder.flashMode == SCFlashMode.Off{
            recorder.flashMode = SCFlashMode.On
        }else{
            recorder.flashMode = SCFlashMode.Off
        }
        
        //self.recorder.pause()
        //self.performSegueWithIdentifier("showVideoPlayer", sender: self)
        //self.uploadVideo()
    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
    let progress: NSProgress = object as! NSProgress
    //progress.removeObserver(self, forKeyPath: "fractionCompleted", context: nil)
    //self.progressBar.setProgress(Float(progress.fractionCompleted), animated: true)
    println("progress: \(progress.fractionCompleted)")
    }

    @IBAction func cancelRecording(sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
        self.progressBar.setProgress(0.0, animated: false)
        //self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func showRecordedVideo(sender: UIButton) {
        
        self.recorder.pause()
        //self.performSegueWithIdentifier("showVideoPlayer", sender: self)
        //self.uploadVideo()
        
    }
    
    func uploadVideo(){

        recordSession?.mergeSegmentsUsingPreset(AVAssetExportPresetHighestQuality, completionHandler:{ (url : NSURL!, error: NSError!) in
            if  url != nil{
                let videoData = NSData(contentsOfURL: url!)
                //self.uploadVideoAlternative(videoData!,videoURl: url)
                GlanceAPISingleton.uploadVideo(videoData!,videoURl: url)
                println("VIDEO URL: \(url)")
                self.videoUrl = url

            }else{
                let alert = UIAlertView()
                alert.title = "Alert"
                alert.message = "You must record a video to review it"
                alert.addButtonWithTitle("Ok")
                alert.show()
            }
        })
    }
    
    func recorder(recorder: SCRecorder!, didAppendVideoSampleBufferInSession session: SCRecordSession!) {
        self.updateProgressBar()
    }

    func updateProgressBar(){
        //println("Update progresss")
        
        var duration:Float64 = CMTimeGetSeconds(recorder.session.duration);
        var maxDuration:Float64 = CMTimeGetSeconds(recorder.maxRecordDuration);
        
        let progression = Float(duration/maxDuration)
        println("progression: \(progression)")
        self.progressBar.setProgress(progression, animated: true)
    }
    
    //Mark - Custom Navigation Controller Transition
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        customNavAnimationController.reverse = operation == .Pop
        return customNavAnimationController
    }
    
    @IBAction func gotoReviewView(sender: UIButton) {
        
        if let viewController = storyboard!.instantiateViewControllerWithIdentifier("reviewView") as? ReviewVideoVC {
            
            self.uploadVideo()
            if self.videoUrl != nil{
                viewController.contentUrl = self.videoUrl
            }
            
            self.tabBarController?.tabBar.hidden = true
            self.animator = ZFModalTransitionAnimator(modalViewController: viewController)
            self.animator.dragable = true
            self.animator.bounces = false
            self.animator.behindViewAlpha = 1.0
            self.animator.behindViewScale = 1.0
            self.animator.transitionDuration = 0.7
            self.animator.direction = ZFModalTransitonDirection.Right
            //self.animator.setContentScrollView(viewController.tableView)
            
            viewController.transitioningDelegate = self.animator
            viewController.modalPresentationStyle = UIModalPresentationStyle.Custom
            //presentViewController(viewController, animated: true, completion: nil)
            presentViewController(viewController, animated: true, completion: { () -> Void in
                self.uploadVideo()
            })
            
//            self.navigationItem.title = ""
//            navigationController?.pushViewController(reviewVideoVC, animated: true)
        }
    }
    
    
    /*override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        if segue!.identifier == "showReviewVC" {
            self.uploadVideo()
            var viewController = segue!.destinationViewController as? ReviewVideoVC
            viewController!.recordSession = self.recordSession
            
        }
    }*/

}
