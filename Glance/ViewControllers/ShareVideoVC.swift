//
//  ShareVideoVC.swift
//  Glance
//
//  Created by Professional on 2015-06-16.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import SABlurImageView

let customCornerRadius:CGFloat = 7.0
class ShareVideoVC: UIViewController {

    @IBOutlet weak var backgroundImage: SABlurImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var cityView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool)
    {
        self.tabBarController?.tabBar.hidden = true
        
        descriptionTextView.layer.cornerRadius = customCornerRadius
        categoryView.layer.cornerRadius = customCornerRadius
        cityView.layer.cornerRadius = customCornerRadius
        self.applyBlurEffect()
        
    }
    
    @IBAction func backToReviewView(){
        
        //navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func applyBlurEffect(){
        backgroundImage.configrationForBlurAnimation()
        backgroundImage?.blur(0.6)
        //backgroundImage.startBlurAnimation(duration: 0.5)
    }
    @IBAction func shareOnFacebook(sender: UIButton) {

        sender.selected = !(sender.selected)
    }
    @IBAction func shareOnTwitter(sender: UIButton) {
        sender.selected = !(sender.selected)
    }
    @IBAction func done(sender: CustomButton) {
        
        var facebookBtn = self.view.viewWithTag(10) as? UIButton
        var twitterBtn = self.view.viewWithTag(11) as? UIButton
        self.tabBarController?.selectedIndex = 0
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
