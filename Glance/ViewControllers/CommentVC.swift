//
//  CommentVC.swift
//  Glance
//
//  Created by Professional on 2015-06-28.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON

class CommentVC: UIViewController {

    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet var tableView: UITableView!
    var comments = [CommentModel]()
     var items: [String] = ["We", "Heart", "Swift","We", "Heart", "Swift","We", "Heart", "Swift"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.cornerRadius = 6
        
        GlanceAPISingleton.fetchComments() { (error, result) -> () in
            //Do your stuff
            self.didReceiveComments(result!)
            self.tableView.reloadData()
        }
        
        //println("background view ---- : \(self.tableView.backgro)")

        
        //self.blurImage()
        
    }

    override func viewWillAppear(animated: Bool) {
        
        self.applyBlurEffect()
    }
    
    func applyBlurEffect(){
        
        //only apply the blur if the user hasn't disabled transparency effects
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.tableView.backgroundColor = UIColor.clearColor()
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.tableView.bounds
            self.tableView.insertSubview(blurEffectView, belowSubview: self.view.viewWithTag(10)!)
            
            //Add Constraints
            blurEffectView.setTranslatesAutoresizingMaskIntoConstraints(false)
            
            let horizontalConstraint = NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.tableView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
            self.tableView.addConstraint(horizontalConstraint)
            
            let verticalConstraint = NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.tableView, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0)
            self.tableView.addConstraint(verticalConstraint)
            
        } else {
            self.tableView.backgroundColor = UIColor.blackColor()
        }
    }

    func didReceiveComments(result: SwiftyJSON.JSON){
        
        println("Received Data")
        //Test
        /*if let userName = result["objects"][0]["video"].string{
        println(userName)
        }*/
        
        var feedsArray = [CommentModel]()
        
        for (index: String, jsonFeed: JSON) in result["objects"] {
            //println(jsonFeed["name"])
            
            /*let stringURL = jsonFeed["thumbnail"].string
            let url = NSURL(string: stringURL!)
            let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
            var image = UIImage(data: data!)*/
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSSxxx"
            
            //Parse date
            let created_datetime = dateFormatter.dateFromString(jsonFeed["created_datetime"].string!) ?? NSDate()
            let modified_datetime = dateFormatter.dateFromString(jsonFeed["modified_datetime"].string!) ?? NSDate()
            
            //Parse longitude and latitude
            let separators = NSCharacterSet(charactersInString: " )(")
            var locationFullString = jsonFeed["location"].string!
            var locationString = String(Array(locationFullString)[6...locationFullString.length-1])
            var words = locationString.componentsSeparatedByCharactersInSet(separators)
            
            var longitude:CGFloat = (CGFloat)((words[1] as NSString).floatValue)
            var latitude:CGFloat = (CGFloat)((words[2] as NSString).floatValue)
            
            let location = CGPointMake(longitude, latitude)
            
            //Parse Population and region
            var population = jsonFeed["population"].int ?? 0
            var region = jsonFeed["region"].int ?? 0
            
            var commentModel = CommentModel(
                active: jsonFeed["active"].bool!,
                pretty_name:jsonFeed["pretty_name"].string!,
                resource_uri:jsonFeed["resource_uri"].string!,
                country_code:jsonFeed["country_code"].string!,
                name:jsonFeed["name"].string!,
                id:jsonFeed["id"].int!,
                population:population,
                region:region,
                created_datetime:modified_datetime,
                modified_datetime:created_datetime//,
                //location:location
            )
            
            feedsArray.append(commentModel)
        }
        // Set our list of new models
        comments = feedsArray
        //cities = realm.objects(CityModel)
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CommentVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        
        //var comment:CommentModel = self.comments[indexPath.row]
        cell.textLabel?.text = "Test"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
    }
}


extension CommentVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        //move textfields up
        let myScreenRect: CGRect = UIScreen.mainScreen().bounds
        let keyboardHeight : CGFloat = 235
        
        UIView.beginAnimations( "animateView", context: nil)
        var movementDuration:NSTimeInterval = 0.5
        var needToMove: CGFloat = 0
        
        var frame : CGRect = self.view.frame
        if (textView.frame.origin.y + textView.frame.size.height +  UIApplication.sharedApplication().statusBarFrame.size.height > (myScreenRect.size.height - keyboardHeight)) {
            needToMove = (textView.frame.origin.y + textView.frame.size.height + UIApplication.sharedApplication().statusBarFrame.size.height) - (myScreenRect.size.height - keyboardHeight);
        }
        
        frame.origin.y = -needToMove
        self.view.frame = frame
        UIView.commitAnimations()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        //move textfields back down
        UIView.beginAnimations( "animateView", context: nil)
        var movementDuration:NSTimeInterval = 0.35
        var frame : CGRect = self.view.frame
        frame.origin.y = 0
        self.view.frame = frame
        UIView.commitAnimations()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        view.endEditing(true)
        return true
    }
}

