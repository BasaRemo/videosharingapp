//
//  InitVC.swift
//  Glance
//
//  Created by Professional on 2015-06-09.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import SABlurImageView

let signInUpPresentTransitionAC = SignInUpPresentTransition()
let signInUpDismissTransitionAC = SignInUpDismissTransition()

class InitVC: UIViewController,UIViewControllerTransitioningDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var backgroundImage: SABlurImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyBlurEffect()
        navigationController?.delegate = self
    }

    override func viewWillAppear(animated: Bool)
    {
        self.navigationController?.navigationBarHidden = true
        self.navigationItem.title = ""
    }
    
    func applyBlurEffect(){
        backgroundImage.configrationForBlurAnimation()
        backgroundImage.startBlurAnimation(duration: 0.5)
    }
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        signInUpPresentTransitionAC.reverse = operation == .Pop
        return signInUpPresentTransitionAC
    }
}

