//
//  SignInUpPresentTransition.swift
//  Glance
//
//  Created by Professional on 2015-06-10.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class SignInUpPresentTransition: NSObject ,UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate{
    
    var reverse: Bool = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        containerView.addSubview(toViewController.view)
        toViewController.view.alpha = 0.0;
        
        UIView.animateWithDuration(0.2, animations: {
            
            toViewController.view.frame = finalFrameForVC
            toViewController.view.alpha = 1;
            }, completion: {
                (value: Bool) in
                transitionContext.completeTransition(true)
        })
        
    }
}
