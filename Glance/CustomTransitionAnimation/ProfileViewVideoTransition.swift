//
//  ProfileViewVideoTransition.swift
//  Glance
//
//  Created by Professional on 2015-07-05.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class ProfileViewVideoTransition: NSObject,UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! CustomTabBarVC
        
        var feedCell: ProfileFeedCell?
        var rowRect:CGRect?
        var thumbnailImage:UIImage?
        
        let profileVC:ProfileVC = fromViewController.viewControllers?[fromViewController.selectedIndex] as! ProfileVC
        feedCell = profileVC.tableView.cellForRowAtIndexPath(profileVC.tableView.indexPathForSelectedRow()!) as? ProfileFeedCell
        
        rowRect = profileVC.tableView.rectForRowAtIndexPath(profileVC.tableView.indexPathForSelectedRow()!)
        var offsetPoint = profileVC.tableView.contentOffset
        // remove the offset from the rowRect
        rowRect!.origin.y -= offsetPoint.y
        // Move to the actual position of the tableView
        rowRect!.origin.x += profileVC.tableView.frame.origin.x
        rowRect!.origin.y += profileVC.tableView.frame.origin.y
        thumbnailImage = feedCell!.videoModel?.image
        
        println("Cell Y: \(rowRect!.origin.y)")
        println("Cell HEIGHT: \(rowRect!.size.height)")
        
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        var backgroundImage = UIImageView(image: thumbnailImage)
        toViewController.view.addSubview(backgroundImage)
        
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        containerView.addSubview(toViewController.view)
        
        let topView = UIView()
        topView.backgroundColor = UIColor.blackColor()
        topView.frame = CGRectMake(0, 0, rowRect!.size.width, rowRect!.origin.y)
        containerView.addSubview(topView)
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor.blackColor()
        bottomView.frame = CGRectMake(0, rowRect!.origin.y + rowRect!.size.height, rowRect!.size.width, rowRect!.size.height)
        containerView.addSubview(bottomView)
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            
            topView.frame = CGRectOffset(topView.frame, 0,-topView.frame.size.height)
            bottomView.frame = CGRectOffset(bottomView.frame, 0, bottomView.frame.size.height)
            
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
                topView.removeFromSuperview()
                bottomView.removeFromSuperview()
                backgroundImage.removeFromSuperview()
        })
    }
}
