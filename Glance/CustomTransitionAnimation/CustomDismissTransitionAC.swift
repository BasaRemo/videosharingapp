//
//  CustomDismissTransitionAC.swift
//  Glance
//
//  Created by Professional on 2015-06-10.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import UIImageViewModeScaleAspect

class CustomDismissTransitionAC: NSObject,UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        //let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as! CustomTabBarVC
        
        let feedVC:FeedsVC = toViewController.viewControllers?[toViewController.selectedIndex] as! FeedsVC
        var feedCell = feedVC.feedsTableView.cellForRowAtIndexPath(feedVC.feedsTableView.indexPathForSelectedRow()!) as! FeedCell
        var thumbnailImage = feedCell.thumbnail.image
        
        var rowRect = feedVC.feedsTableView.rectForRowAtIndexPath(feedVC.feedsTableView.indexPathForSelectedRow()!)
        var offsetPoint = feedVC.feedsTableView.contentOffset
        // remove the offset from the rowRect
        rowRect.origin.y -= offsetPoint.y;
        // Move to the actual position of the tableView
        rowRect.origin.x += feedVC.feedsTableView.frame.origin.x;
        rowRect.origin.y += feedVC.feedsTableView.frame.origin.y;
        
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        containerView.addSubview(toViewController.view)
        containerView.sendSubviewToBack(toViewController.view)
        
        UIGraphicsBeginImageContextWithOptions(fromViewController.view.frame.size, false, 0)
        var image:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        fromViewController.view.drawViewHierarchyInRect(fromViewController.view.frame, afterScreenUpdates: true)
        var screenShot  = UIGraphicsGetImageFromCurrentImageContext()
        
        var backgroundImage = UIImageViewModeScaleAspect(frame: fromViewController.view.frame)
        backgroundImage.contentMode = .ScaleAspectFill
        backgroundImage.image = screenShot
        containerView.addSubview(backgroundImage)
        fromViewController.view.removeFromSuperview()
        
        backgroundImage.animateToScaleAspectFillToFrame(rowRect, withDuration: 1.0, afterDelay: 0, completion: {
            finished in
            transitionContext.completeTransition(true)
            //backgroundImage.removeFromSuperview()
            
        })
        
//        let topView = UIView()
//        topView.backgroundColor = UIColor.blackColor()
//        topView.frame = CGRectMake(0, 0-rowRect.origin.y, rowRect.size.width, rowRect.origin.y)
//        containerView.addSubview(topView)
//        
//        let bottomView = UIView()
//        bottomView.backgroundColor = UIColor.blackColor()
//        bottomView.frame = CGRectMake(0, rowRect.origin.y + rowRect.size.height + rowRect.size.height , rowRect.size.width, rowRect.size.height)
//        containerView.addSubview(bottomView)
//        
//        
//        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
//            snapshotView.frame = rowRect
//            
//            topView.frame = CGRectOffset(topView.frame, 0,topView.frame.size.height)
//            bottomView.frame = CGRectOffset(bottomView.frame, 0, -bottomView.frame.size.height)
//            }, completion: {
//                finished in
//                topView.removeFromSuperview()
//                bottomView.removeFromSuperview()
//                snapshotView.removeFromSuperview()
//                transitionContext.completeTransition(true)
//        })
    }
}
