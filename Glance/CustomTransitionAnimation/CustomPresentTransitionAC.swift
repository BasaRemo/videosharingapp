//
//  CustomPresentTransitionAC.swift
//  Glance
//
//  Created by Professional on 2015-06-10.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import QuartzCore
import UIImageViewModeScaleAspect

class CustomPresentTransitionAC: NSObject,UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate {
   
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! CustomTabBarVC
        
        var feedCell: FeedCell?
        var rowRect:CGRect?
        var thumbnailImage:UIImage?
        
        if  fromViewController.viewControllers?[fromViewController.selectedIndex] is FeedsVC {
            let feedVC:FeedsVC = fromViewController.viewControllers?[fromViewController.selectedIndex] as! FeedsVC
            feedCell = feedVC.feedsTableView.cellForRowAtIndexPath(feedVC.feedsTableView.indexPathForSelectedRow()!) as? FeedCell
            
            rowRect = feedVC.feedsTableView.rectForRowAtIndexPath(feedVC.feedsTableView.indexPathForSelectedRow()!)
            
            var offsetPoint = feedVC.feedsTableView.contentOffset
            // remove the offset from the rowRect
            rowRect!.origin.y -= offsetPoint.y;
            // Move to the actual position of the tableView
            rowRect!.origin.x += feedVC.feedsTableView.frame.origin.x;
            rowRect!.origin.y += feedVC.feedsTableView.frame.origin.y;
            
            thumbnailImage = feedCell!.videoModel?.image
            //var rectInSuperview = feedVC.feedsTableView.convertRect(rowRect, toView: feedVC.feedsTableView.superview)
            println("Cell Y: \(rowRect!.origin.y)")
            println("Cell HEIGHT: \(rowRect!.size.height)")
            
        }else if fromViewController.viewControllers?[fromViewController.selectedIndex] is ProfileVC {
            
            let profileVC:ProfileVC = fromViewController.viewControllers?[fromViewController.selectedIndex] as! ProfileVC
            feedCell = profileVC.tableView.cellForRowAtIndexPath(profileVC.tableView.indexPathForSelectedRow()!) as? FeedCell
            
            rowRect = profileVC.tableView.rectForRowAtIndexPath(profileVC.tableView.indexPathForSelectedRow()!)
            var offsetPoint = profileVC.tableView.contentOffset
            // remove the offset from the rowRect
            rowRect!.origin.y -= offsetPoint.y
            // Move to the actual position of the tableView
            rowRect!.origin.x += profileVC.tableView.frame.origin.x
            rowRect!.origin.y += profileVC.tableView.frame.origin.y
            //thumbnailImage = feedCell!.thumbnailImage!
        }
        
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as! VideoPlayerVC
        toViewController.thumbnail = thumbnailImage
        
        toViewController.imageThumb = UIImageViewModeScaleAspect(frame: rowRect!)
        toViewController.imageThumb!.contentMode = .ScaleAspectFill
        toViewController.imageThumb!.image = thumbnailImage
        /*var backgroundImage = UIImageViewModeScaleAspect(frame: rowRect!)
        backgroundImage.contentMode = .ScaleAspectFill
        backgroundImage.image = thumbnailImage*/
        //var backgroundImage = UIImageView(image: thumbnailImage)
        toViewController.view.addSubview(toViewController.imageThumb!)
        
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        containerView.addSubview(toViewController.view)
        
//        let topView = UIView()
//        topView.backgroundColor = UIColor.blackColor()
//        topView.frame = CGRectMake(0, 0, rowRect!.size.width, rowRect!.origin.y)
//        //containerView.addSubview(topView)
//        
//        let bottomView = UIView()
//        bottomView.backgroundColor = UIColor.blackColor()
//        bottomView.frame = CGRectMake(0, rowRect!.origin.y + rowRect!.size.height, rowRect!.size.width, rowRect!.size.height)
        //containerView.addSubview(bottomView)
        toViewController.imageThumb!.animateToScaleAspectFillToFrame(toViewController.view.frame, withDuration: 0.6, afterDelay: 0, completion: {
            finished in
            transitionContext.completeTransition(true)
        })
        /*UIView.animateWithDuration(transitionDuration(transitionContext), animations: {

            //topView.frame = CGRectOffset(topView.frame, 0,-topView.frame.size.height)
            //bottomView.frame = CGRectOffset(bottomView.frame, 0, bottomView.frame.size.height)
            
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
                //topView.removeFromSuperview()
                //bottomView.removeFromSuperview()
                //backgroundImage.removeFromSuperview()
        })*/
    }
    
}
