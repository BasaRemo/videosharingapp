//
//  ProfileViewVideoDismiss.swift
//  Glance
//
//  Created by Professional on 2015-07-05.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class ProfileViewVideoDismiss: NSObject,UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        //let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as! CustomTabBarVC
        
        let feedVC:ProfileVC = toViewController.viewControllers?[toViewController.selectedIndex] as! ProfileVC
        var feedCell = feedVC.tableView.cellForRowAtIndexPath(feedVC.tableView.indexPathForSelectedRow()!) as! ProfileFeedCell
        
        var rowRect = feedVC.tableView.rectForRowAtIndexPath(feedVC.tableView.indexPathForSelectedRow()!)
        var offsetPoint = feedVC.tableView.contentOffset
        // remove the offset from the rowRect
        rowRect.origin.y -= offsetPoint.y;
        // Move to the actual position of the tableView
        rowRect.origin.x += feedVC.tableView.frame.origin.x;
        rowRect.origin.y += feedVC.tableView.frame.origin.y;
        
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        containerView.addSubview(toViewController.view)
        containerView.sendSubviewToBack(toViewController.view)
        
        let snapshotView = fromViewController.view.snapshotViewAfterScreenUpdates(false)
        snapshotView.frame = fromViewController.view.frame
        containerView.addSubview(snapshotView)
        fromViewController.view.removeFromSuperview()
        
        let topView = UIView()
        topView.backgroundColor = UIColor.blackColor()
        topView.frame = CGRectMake(0, 0-rowRect.origin.y, rowRect.size.width, rowRect.origin.y)
        containerView.addSubview(topView)
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor.blackColor()
        bottomView.frame = CGRectMake(0, rowRect.origin.y + rowRect.size.height + rowRect.size.height , rowRect.size.width, rowRect.size.height)
        containerView.addSubview(bottomView)
        
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            //snapshotView.frame = rowRect
            topView.frame = CGRectOffset(topView.frame, 0,topView.frame.size.height)
            bottomView.frame = CGRectOffset(bottomView.frame, 0, -bottomView.frame.size.height)
            }, completion: {
                finished in
                topView.removeFromSuperview()
                bottomView.removeFromSuperview()
                snapshotView.removeFromSuperview()
                transitionContext.completeTransition(true)
        })
    }
}
