//
//  VideoTransition.swift
//  Glance
//
//  Created by Professional on 2015-08-02.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import Foundation
import QuartzCore

class VideoTransition: NSObject,UIViewControllerAnimatedTransitioning,UIViewControllerTransitioningDelegate {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! CustomTabBarVC
        
        var feedCell: FeedCell?
        var rowRect:CGRect?
        var thumbnailImage:UIImage?
        
        if  fromViewController.viewControllers?[fromViewController.selectedIndex] is FeedsVC {
            let feedVC:FeedsVC = fromViewController.viewControllers?[fromViewController.selectedIndex] as! FeedsVC
            feedCell = feedVC.feedsTableView.cellForRowAtIndexPath(feedVC.feedsTableView.indexPathForSelectedRow()!) as? FeedCell
            
            rowRect = feedVC.feedsTableView.rectForRowAtIndexPath(feedVC.feedsTableView.indexPathForSelectedRow()!)
            
            var offsetPoint = feedVC.feedsTableView.contentOffset
            // remove the offset from the rowRect
            rowRect!.origin.y -= offsetPoint.y;
            // Move to the actual position of the tableView
            rowRect!.origin.x += feedVC.feedsTableView.frame.origin.x;
            rowRect!.origin.y += feedVC.feedsTableView.frame.origin.y;
            
            thumbnailImage = feedCell!.videoModel?.image
            //var rectInSuperview = feedVC.feedsTableView.convertRect(rowRect, toView: feedVC.feedsTableView.superview)
            println("Cell Y: \(rowRect!.origin.y)")
            println("Cell HEIGHT: \(rowRect!.size.height)")
            
        }else if fromViewController.viewControllers?[fromViewController.selectedIndex] is ProfileVC {
            
            let profileVC:ProfileVC = fromViewController.viewControllers?[fromViewController.selectedIndex] as! ProfileVC
            feedCell = profileVC.tableView.cellForRowAtIndexPath(profileVC.tableView.indexPathForSelectedRow()!) as? FeedCell
            
            rowRect = profileVC.tableView.rectForRowAtIndexPath(profileVC.tableView.indexPathForSelectedRow()!)
            var offsetPoint = profileVC.tableView.contentOffset
            // remove the offset from the rowRect
            rowRect!.origin.y -= offsetPoint.y
            // Move to the actual position of the tableView
            rowRect!.origin.x += profileVC.tableView.frame.origin.x
            rowRect!.origin.y += profileVC.tableView.frame.origin.y
            //thumbnailImage = feedCell!.thumbnailImage!
        }
        
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        var backgroundImage = UIImageView(image: thumbnailImage)
        toViewController.view.addSubview(backgroundImage)
        //toViewController.view.backgroundColor = UIColor.redColor()
        
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrameForVC
        containerView.addSubview(toViewController.view)
        
        /*let topView = UIView()
        topView.backgroundColor = UIColor.blackColor()
        topView.frame = CGRectMake(0, 0, rowRect!.size.width, rowRect!.origin.y)
        containerView.addSubview(topView)
        
        let bottomView = UIView()
        bottomView.backgroundColor = UIColor.blackColor()
        bottomView.frame = CGRectMake(0, rowRect!.origin.y + rowRect!.size.height, rowRect!.size.width, rowRect!.size.height)
        containerView.addSubview(bottomView)*/
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            
            //topView.frame = CGRectOffset(topView.frame, 0,-topView.frame.size.height)
            //bottomView.frame = CGRectOffset(bottomView.frame, 0, bottomView.frame.size.height)
            
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
                backgroundImage.removeFromSuperview()
        })
    }
    
}
