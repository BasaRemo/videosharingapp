//
//  VideoFeedModel.swift
//  Glance
//
//  Created by Professional on 2015-06-11.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import Haneke

//Create the delegate protocol
protocol VideoDelegate {
    func imageDownloaded()
    //func didSucceededLogin()
}


class VideoFeedModel:NSObject {
    
    var video: String?
    var thumbnailUrl: String?
    var image:UIImage?
    var delegate: VideoDelegate?
    
    init(video: String,thumbnailUrl:String) {
        self.video   = video
        self.thumbnailUrl = thumbnailUrl
    }
    
    func downloadImage(){
        let cache = Shared.imageCache
        
        let iconFormat = Format<UIImage>(name: "icons", diskCapacity: 10 * 1024 * 1024) { image in
            return image
        }
        cache.addFormat(iconFormat)
        
        let URL = NSURL(string: thumbnailUrl!)!
        cache.fetch(URL: URL, formatName: "icons").onSuccess { image in
            self.image = image
            println("Image downloaded: \(image)")
        }
    }
}
