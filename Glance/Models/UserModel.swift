//
//  UserModel.swift
//  Glance
//
//  Created by Professional on 2015-06-11.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit

class UserModel {
    
    var id,
    phoneNumber:Int?
    var date_joined:NSDate?
    var fullName,
    resource_uri,
    first_name,
    city,
    pretty_username,
    username,
    last_name,
    email:String?
    
    init(fullName: String, resource_uri: String, first_name: String,city: String, pretty_username: String, username: String,last_name: String,email: String,id: Int,phoneNumber: Int,date_joined: NSDate) {
        
        self.fullName   = fullName
        self.resource_uri = resource_uri
        self.first_name  = first_name
        self.city   = city
        self.pretty_username = pretty_username
        self.username  = username
        self.last_name   = last_name
        self.email = email
        self.id  = id
        self.phoneNumber   = phoneNumber
        self.date_joined = date_joined
    }
}
