//
//  LikeModel.swift
//  Glance
//
//  Created by Professional on 2015-06-29.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//

import UIKit
import RealmSwift
import Realm
class LikeModel: Object {
    
    dynamic var active = false
    dynamic var id = 0
    dynamic var population = 0
    dynamic var region = 0
    dynamic var created_datetime:NSDate?
    dynamic var modified_datetime:NSDate?
    //dynamic var location = CGPointMake(0, 0)
    
    dynamic var pretty_name  = ""
    dynamic var resource_uri  = ""
    dynamic var country_code  = ""
    dynamic var name = ""
    
    required init() {
        super.init()
    }
    
    override init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    convenience init(active:Bool,pretty_name: String, resource_uri: String, country_code: String,name: String,id: Int,population: Int,region: Int,created_datetime: NSDate,modified_datetime: NSDate) {
        self.init()
        self.active   = active
        self.pretty_name = pretty_name
        self.resource_uri  = resource_uri
        self.country_code   = country_code
        self.name = name
        self.id  = id
        self.population   = population
        self.region = region
        self.created_datetime  = created_datetime
        self.modified_datetime   = modified_datetime
        //self.location = location
    }
    
}

