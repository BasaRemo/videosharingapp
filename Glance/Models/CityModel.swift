//
//  CityModel.swift
//  Glance
//
//  Created by Professional on 2015-06-11.
//  Copyright (c) 2015 Ntambwa. All rights reserved.
//
/* City request Example
    {
        "active" : true,
        "location" : "POINT (25.3166669999999989 -100.0499999999999972)",
        "modified_datetime" : "2015-03-22T02:14:11.669882",
        "id" : 5062114,
        "created_datetime" : "2015-03-22T02:14:11.669851",
        "population" : "",
        "pretty_name" : "Lazarillos",
        "region" : "19",
        "country_code" : "MX",
        "resource_uri" : "\/api\/1.0\/city\/5062114\/",
        "name" : "lazarillos"
    }
*/

import UIKit
import RealmSwift
import Realm
class CityModel: Object {

    dynamic var active = false
    dynamic var id = 0
    dynamic var population = 0
    dynamic var region = 0
    dynamic var created_datetime:NSDate?
    dynamic var modified_datetime:NSDate?
    //dynamic var location = CGPointMake(0, 0)
    
    dynamic var pretty_name  = ""
    dynamic var resource_uri  = ""
    dynamic var country_code  = ""
    dynamic var name = ""
    
    required init() {
        super.init()
    }
    
    override init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    convenience init(active:Bool,pretty_name: String, resource_uri: String, country_code: String,name: String,id: Int,population: Int,region: Int,created_datetime: NSDate,modified_datetime: NSDate) {
        self.init()
        self.active   = active
        self.pretty_name = pretty_name
        self.resource_uri  = resource_uri
        self.country_code   = country_code
        self.name = name
        self.id  = id
        self.population   = population
        self.region = region
        self.created_datetime  = created_datetime
        self.modified_datetime   = modified_datetime
        //self.location = location
    }

}
